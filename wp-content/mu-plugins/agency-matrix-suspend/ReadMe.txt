=== AgencyMatrix-Suspend ===
Contributors: Team Prometheus

== Description ==

In short, this plugin serves to automate the process of enabling and disabling the under construction plugin should an account be suspended or activated respectively. 

== Installation ==

1. Upload the entire `agencymatrix-suspend` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. The plugin will redirect you to the coming-soon settings. Under Status: Select 'Enable Maintenance Mode'.

