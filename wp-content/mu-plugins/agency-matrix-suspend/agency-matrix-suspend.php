<?php
    /**
     * Plugin Name: Agency Matrix Suspend
     * Plugin URI: http://www.agencymatrix.com/
     * Description: Enable and disable "Coming Soon Page & Maintenance Mode by SeedProd" under construction plugin from within Agency Matrix to automate suspending client websites.
     * Version: 2.0
     * Author: Jahmal Newton
     * Author URI: http://www.agencymatrix.com/
     */

    /**
     * Create a restful api route & grab the state of the AM account
     *
     */
    function am_handleUrl(WP_REST_Request $request) {
        $plugin = 'coming-soon/coming-soon.php';
        $active = $request->get_param('active');

        $active ? deactivate_plugins($plugin) : activate_plugins($plugin);

        $response = new WP_REST_Response([
            'state' => $active ? 'inactive' : 'active'
        ]);

        return $response;
    }

    //register route to set up url to handle response
    add_action( 'rest_api_init', function ()
    {
        register_rest_route( 'am', '/suspend', array(
            'methods' => 'GET',
            'callback' => 'am_handleUrl',
            'args' => array(
                'active' => array(
                    'sanitize_callback' => function($param) {
                        return boolval($param);
                    }
                ),
            )
        ) );
    } );