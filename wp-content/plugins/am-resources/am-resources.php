<?php
/*
Plugin Name: Agency Matrix Resources
Plugin URI: http://www.agencymatrix.com/
Description: Resources Plugin. <a href="http://www.advancedcustomfields.com" target="_blank">Requires Advanced Custom Fields Plugin</a>
Version: 1.0
Author: Agency Matrix
Author URI: http://www.agencymatrix.com/
License: Proprietary
*/
function am_resources_post_type()
{
    add_rewrite_endpoint(
        'resources',
        EP_PAGES
    );

    if(get_transient( 'vpt_flush' )) {
        delete_transient( 'vpt_flush' );
        flush_rewrite_rules();
    }

    am_resources_post_type_register();

    register_post_type( 'am_resource',
        array(
            'labels' => array(
                'name' => __( 'Resources' ),
                'singular_name' => __( 'Resource' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
}

function am_resources_post_type_register()
{
    if (!is_plugin_active('advanced-custom-fields-pro/acf.php')) {
        throw new \Exception('Resources Plugin Requires The Advanced Custom Fields Plugin');
    }

    set_transient( 'vpt_flush', 1, 60 );
}

function am_resources_table_list()
{
    ob_start();

    require_once __DIR__ . '/page.phtml';

    $contents = ob_get_contents();
    ob_end_clean();

    return $contents;
}

function am_resources_packet()
{
    if (!($resourceLanguage = get_query_var('resources', false))) {
        return;
    } else if (!in_array($resourceLanguage, ['en', 'sp'])) {
        wp_redirect(get_permalink(get_post()));
    }

    $packetFiles = [];

    while (have_rows('resources')) {
        the_row();

        $resourcePost = get_sub_field('resource');

        $filePath = get_field($resourceLanguage === 'en' ? 'english_file' : 'spanish_file', $resourcePost);
        if (!$filePath || !isset($filePath['ID'])) {
            continue;
        }

        $filePath = get_attached_file($filePath['ID']);

        if ($filePath) {
            $packetFiles[] = $filePath;
        }
    }

    $filePath = am_create_zip($packetFiles);

    header('Content-disposition: attachment; filename=' . get_post()->post_name . '.zip');
    header("Content-type: application/octet-stream");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ". filesize($filePath));
    readfile($filePath);

    unlink($filePath);

    exit;
}

function am_create_zip($packetFiles)
{
    $filePath = wp_upload_dir()['path'] . '/' . get_post()->post_name . '.' . time() . '.zip';

    $zipArchive = new ZipArchive();
    if (!$zipArchive->open($filePath,ZipArchive::CREATE)) {
        return 'Failure';
    }

    foreach($packetFiles as $packetFile) {
        $zipArchive->addFile($packetFile, basename($packetFile));
    }

    $zipArchive->close();

    return $filePath;
}

add_action('init', 'am_resources_post_type');
add_shortcode(
    'am_resource_table',
    'am_resources_table_list'
);

add_action(
    'template_redirect',
    'am_resources_packet'
);

register_activation_hook(
    __FILE__,
    'am_resources_post_type_register'
);

//TEMP

add_filter('pre_site_option_ub_from_email', function() {
	return 'vensure@vensureinc.com';
});

add_filter('pre_site_option_ub_from_name', function() {
	return 'Vensure Contact Form';
});